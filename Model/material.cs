//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class material
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public material()
        {
            this.document_detail = new HashSet<document_detail>();
            this.listprice_detail = new HashSet<listprice_detail>();
        }
    
        public int material_id { get; set; }
        public string material_description { get; set; }
        public int product_category_id { get; set; }
        public int unit_measure_id { get; set; }
        public string material_type { get; set; }
        public Nullable<int> material_attribute_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<document_detail> document_detail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<listprice_detail> listprice_detail { get; set; }
        public virtual unit_measure unit_measure { get; set; }
        public virtual product_category product_category { get; set; }
    }
}
