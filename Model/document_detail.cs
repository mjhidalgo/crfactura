//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class document_detail
    {
        public int document_detail_id { get; set; }
        public Nullable<decimal> document_detail_quantity { get; set; }
        public Nullable<decimal> document_detail_amount { get; set; }
        public string document_detail_currency { get; set; }
        public Nullable<decimal> document_detail_tax { get; set; }
        public Nullable<decimal> document_detail_discount { get; set; }
        public Nullable<decimal> document_detail_netamount { get; set; }
        public Nullable<decimal> document_detail_grossamount { get; set; }
        public Nullable<decimal> document_detail_taxamount { get; set; }
        public int material_id { get; set; }
        public int document_id { get; set; }
    
        public virtual document document { get; set; }
        public virtual material material { get; set; }
    }
}
