//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class document
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public document()
        {
            this.document_detail = new HashSet<document_detail>();
            this.document_signed = new HashSet<document_signed>();
        }
    
        public int document_id { get; set; }
        public string document_number { get; set; }
        public Nullable<System.DateTime> document_date { get; set; }
        public int customer_id { get; set; }
        public Nullable<decimal> document_totalnetamount { get; set; }
        public Nullable<decimal> document_totalgrossamount { get; set; }
        public Nullable<decimal> document_totaltaxamount { get; set; }
        public string document_currency { get; set; }
        public int document_type_id { get; set; }
        public int payment_method_id { get; set; }
        public int sales_condition_id { get; set; }
    
        public virtual customer customer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<document_detail> document_detail { get; set; }
        public virtual document_type document_type { get; set; }
        public virtual payment_method payment_method { get; set; }
        public virtual sales_condition sales_condition { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<document_signed> document_signed { get; set; }
    }
}
