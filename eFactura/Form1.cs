﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using FirmaXadesNet;
using FirmaXadesNet.Signature;
using FirmaXadesNet.Signature.Parameters;
using FirmaXadesNet.Crypto;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using RestSharp;
using FirmaXadesNet.Upgraders;
using System.Security.Cryptography;

namespace eFactura
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txtCert.Text = @"C:\Users\Melvin Hidalgo\Desktop\FacturaElectronica\Clave.p12";
            txtXML.Text = @"C:\Users\Melvin Hidalgo\Desktop\FacturaElectronica\factura.xml";
        }



        private void button1_Click(object sender, EventArgs e)
        {

            SendHacienda(SignDocument(), lblToken.Text);

        }
        public String getDigestPDF()
        {



            Byte[] inputData = File.ReadAllBytes(@"C:\Users\Melvin Hidalgo\Desktop\FacturaElectronica\ResolucionComprobantesElectronicosDGT-R-48-2016_4.2.pdf");


            SHA256 sha256 = new SHA256CryptoServiceProvider();
            byte[] hash = sha256.ComputeHash(inputData);



            String hashString = Convert.ToBase64String(hash);
            return hashString;

        }
        public String GetRespuesta(String ClaveID, String access_token)
        {

            String rsp = "";
            String URL = "https://api.comprobanteselectronicos.go.cr/recepcion-sandbox/v1/recepcion/" + ClaveID;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
            // httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.Headers.Add("Authorization", "bearer " + access_token);



            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                try
                {
                    String data = JObject.Parse(result)["respuesta-xml"].ToString();
                    rsp = desencriptarBase64(data);
                    string path = @"C:\Users\Melvin Hidalgo\Desktop\FacturaElectronica\responseHacienda.xml";
                    string createText = rsp;
                    File.WriteAllText(path, createText);
                    MessageBox.Show("Response saved");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);

                }
            }

            return rsp;


        }


        public String getAccessToken()
        {
            String accessToken = "";
            String serviceURL = "https://idp.comprobanteselectronicos.go.cr/auth/realms/rut-stag/protocol/openid-connect/token";
            String IDP_CLIENT_ID = "api-stag";
            String usuario = "cpf-01-1523-0304@stag.comprobanteselectronicos.go.cr";
            String password = "jYUk]%Vp{+.}@8$P%2=5";
            using (WebClient client = new WebClient())
            {
                try
                {
                    byte[] response =
                    client.UploadValues(serviceURL, new NameValueCollection()
                    {
                   { "grant_type", "password" },
                   { "username", usuario },
                   { "password", password },
                   { "client_id", IDP_CLIENT_ID },
                           });

                    String result = System.Text.Encoding.UTF8.GetString(response);
                    dynamic data = JObject.Parse(result);

                    accessToken = data.access_token;
                }
                catch (Exception e) {

                    MessageBox.Show("No se pudo retornar el token, intentar nuevamente");

                }

            }

            return accessToken;

        }

        public String SignDocument()
        {
            String saveSigned = @"C:\Users\Melvin Hidalgo\Desktop\FacturaElectronica\facturaFirmado.xml";
            String text = "";
            XadesService _xadesService = new XadesService();

            SignatureDocument _signatureDocument;

            if (!string.IsNullOrEmpty(txtCert.Text))
            {
                SignaturePolicyInfo spi = new SignaturePolicyInfo();
                spi.PolicyIdentifier = "https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.1/Resolucion_Comprobantes_Electronicos_DGT-R-48-2016.pdf";
                spi.PolicyHash = "eUGBLxo7SaqxaR+CGU5DCjUc514GOiOU9S0Smy5W7HE=";
                X509Certificate2 cert = new X509Certificate2(@txtCert.Text, "4681");
                SignatureParameters parametros = new SignatureParameters();
                parametros.SignatureMethod = SignatureMethod.RSAwithSHA256;
                parametros.DigestMethod = DigestMethod.SHA256;
                parametros.SigningDate = DateTime.Now;
                parametros.SignaturePackaging = SignaturePackaging.ENVELOPED;
                parametros.SignaturePolicyInfo = spi;


                using (parametros.Signer = new Signer(cert))
                {
                    using (FileStream fs = new FileStream(txtXML.Text, FileMode.Open))
                    {
                        _signatureDocument = _xadesService.Sign(fs, parametros);
                    }

                }

                _signatureDocument.Save(saveSigned);

                text = System.IO.File.ReadAllText(saveSigned);




            }
            return text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtXML.Text = openFileDialog1.FileName;
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtCert.Text = openFileDialog1.FileName;
            }
        }

        public String getIDfromXML(String XML)
        {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(XML);
            return doc.GetElementsByTagName("Clave")[0].InnerText;
        }

        public void SendHacienda(String xmlFirmado, String access_token)
        {
            String ID = getIDfromXML(xmlFirmado);
            String encodedXML = encriptarBase64(xmlFirmado);
            String json = "{\n" +
                 "  \"clave\": \"" + ID + "\",\n" +
                 "  \"fecha\": \"2017-28-12T11:00:00-0600\",\n" +
                 "  \"emisor\": {\n" +
                 "    \"tipoIdentificacion\": \"01\",\n" +
                 "    \"numeroIdentificacion\": \"115230304\"\n" +
                 "  },\n" +
                 "  \"receptor\": {\n" +
                 "    \"tipoIdentificacion\": \"02\",\n" +
                 "    \"numeroIdentificacion\": \"3001123208\"\n" +
                 "  },\n" +
                 "  \"comprobanteXml\": \"" + encodedXML + "\"\n" +
                 "}";

            string path = @"C:\Users\Melvin Hidalgo\Desktop\FacturaElectronica\JSON.json";

            string createText = json;
            File.WriteAllText(path, createText);

            String URL = "https://api.comprobanteselectronicos.go.cr/recepcion-sandbox/v1/recepcion";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "bearer " + access_token);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            MessageBox.Show(httpResponse.StatusCode.ToString());
            txtClave.Text = ID;




        }


        public String encriptarBase64(String texto)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(texto);
            return System.Convert.ToBase64String(plainTextBytes);

        }

        public String desencriptarBase64(String texto)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(texto);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);

        }

        private void button4_Click(object sender, EventArgs e)
        {

            GetRespuesta(txtClave.Text, lblToken.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            lblToken.Text = getAccessToken();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
